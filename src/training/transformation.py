
from training import transforms as T, HYPERPARAMS

# Trainsformationen welche auf die Bilder angewendet werden
# Umwandlung in einen Tensor ung gegebenfalls Image augmentation
def get_transform(train):
    transforms = []
    transforms.append((T.ToTensor()))
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
        transforms.append(T.RandomColorJitter(prob=HYPERPARAMS.RANDOM_COLOR_JITTING,
                                              brightness=HYPERPARAMS.BRIGHTNESS,
                                              contrast=HYPERPARAMS.CONTRAST,
                                              saturation=HYPERPARAMS.SATURATION,
                                              hue=HYPERPARAMS.HUE))
    return T.Compose(transforms)
