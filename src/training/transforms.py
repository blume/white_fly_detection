import random

from torchvision.transforms import functional as F


def _flip_coco_person_keypoints(kps, width):
    flip_inds = [0, 2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 12, 11, 14, 13, 16, 15]
    flipped_data = kps[:, flip_inds]
    flipped_data[..., 0] = width - flipped_data[..., 0]
    # Maintain COCO convention that if visibility == 0, then x, y = 0
    inds = flipped_data[..., 2] == 0
    flipped_data[inds] = 0
    return flipped_data


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)
        return image, target


class RandomHorizontalFlip(object):
    def __init__(self, prob):
        self.prob = prob

    def __call__(self, image, target):
        if random.random() < self.prob:
            height, width = image.shape[-2:]
            image = image.flip(-1)
            bbox = target["boxes"]
            bbox[:, [0, 2]] = width - bbox[:, [2, 0]]
            target["boxes"] = bbox
            if "masks" in target:
                target["masks"] = target["masks"].flip(-1)
            if "keypoints" in target:
                keypoints = target["keypoints"]
                keypoints = _flip_coco_person_keypoints(keypoints, width)
                target["keypoints"] = keypoints
        return image, target

# Eigne funktionale Implementierung von Random Color Jittering
class RandomColorJitter(object):
    def __init__(self, prob, brightness, contrast, saturation, hue):
        self.prob = prob
        self.brightness = brightness
        self.contrast = contrast
        self.saturation = saturation
        self.hue = hue

    def __call__(self, image, target):
        if self.prob > random.random():
            image = F.adjust_brightness(image, randomize(self.brightness))
            image = F.adjust_contrast(image, randomize(self.contrast))
            image = F.adjust_saturation(image, randomize(self.saturation))
            image = F.adjust_hue(image, random.uniform(-1 * self.hue, self.hue))
        return image, target


def randomize(value) -> float:
    return 1 + random.uniform(-1 * value, value)


class ToTensor(object):
    def __call__(self, image, target):
        image = F.to_tensor(image)
        return image, target
