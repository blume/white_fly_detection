import os
import torch
import torch.utils.data
import xml.etree.ElementTree as ET
from PIL import Image, ImageOps


# Klasse zum Laden des Datensets
class WhiteFlyDataset(torch.utils.data.Dataset):
    def __init__(self, root, transforms):
        self.root = root
        self.transforms = transforms
        self.imgs = list(sorted(os.listdir(os.path.join(root, "images"))))
        self.__annotations__ = list(sorted(os.listdir(os.path.join(root, "annotations"))))





    # Funktion zum Laden von Bildern und Annotations Files (was detektiert werden soll)
    def __getitem__(self, index):
        img_path = os.path.join(self.root, "images", self.imgs[index])
        annotation_path = os.path.join(self.root, "annotations", self.__annotations__[index])
        img = Image.open(img_path)
        img = ImageOps.exif_transpose(img)
        tree = ET.parse(annotation_path)
        root = tree.getroot()

        count_objects = 0
        boxes = []
        # Laden der Koordinaten der boundingboxes aus den Image Files
        for object in root.iter('object'):
            count_objects += 1

            ymin = int(object.find("bndbox/ymin").text)
            xmin = int(object.find("bndbox/xmin").text)
            ymax = int(object.find("bndbox/ymax").text)
            xmax = int(object.find("bndbox/xmax").text)
            
            if ymin == ymax or xmin == xmax:
                print(img_path, "has box with illegal size (0)")
            else:
                boxes.append([xmin, ymin, xmax, ymax])
        
        if count_objects > 0:
            boxes = torch.as_tensor(boxes, dtype=torch.int16)
        else:
            boxes = torch.zeros((0, 4), dtype=torch.int16)

        labels = torch.ones(count_objects, dtype=torch.int64)
        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((count_objects,), dtype=torch.int16)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd



        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)
