import torch
import torchvision
import io
from model.pretrained_model import get_pretrained_model
#from ..common.pretrained_model import get_pretrained_model
from flask import Flask, jsonify, request, send_file, make_response
from torchvision.transforms import transforms
from PIL import Image, ImageOps
from torchvision.transforms.functional import to_pil_image
from torchvision.utils import draw_bounding_boxes


# Webanwendung in welche das Deeplearing Modell integriert ist

app = Flask(__name__)
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

# Laden des Modells
model = get_pretrained_model(2)
model.load_state_dict(torch.load("../models/model_2021-09-08_08-12/state-dict_epoch-5", map_location=device))
model.eval()

# Drehen des Bildes entsprechend der Metadaten und Umwandlung in einen Tensor
def image_preprocessing(img_file):
    input_transforms = [
        transforms.ToTensor()
    ]
    transformations = transforms.Compose(input_transforms)
    img = Image.open(img_file).convert('RGB')
    img = ImageOps.exif_transpose(img)
    transformed_img = transformations(img)
    return transformed_img


# Detektion der Weissen Fliege
def get_prediction(input_tensor):
    with torch.no_grad():
        prediction = model([input_tensor])
    return prediction


# Markieren der detekierten Weissen Fliegen
def get_prediction_on_image(input_tensor, prediction_tensor):
    img = input_tensor.mul(255)
    img = torch.as_tensor(img, dtype=torch.uint8)
    boxes = torch.as_tensor(prediction_tensor[0]['boxes'])
    labels = torch.as_tensor(prediction_tensor[0]['labels']).tolist()
    labels = [str(label) for label in labels]
    img_with_predictions = draw_bounding_boxes(img, boxes, labels)
    img_with_predictions = to_pil_image(img_with_predictions)
    return img_with_predictions.convert('RGB')


@app.route('/', methods=['GET'])
def root():
    return jsonify({'msg' : 'Try POSTing images to /predictimage or predictjson'})


# Endpunkt zur Rückgabe der JSON Datei mit denn Markierungen der Weissen Fliegen
@app.route('/predictjson', methods=['POST'])
def predictjson():
    if request.method == 'POST':
        file = request.files['file']
        if file is not None:
            prediction = get_prediction(image_preprocessing(file))
            boxes = torch.as_tensor(prediction[0]['boxes']).tolist()
            labels = torch.as_tensor(prediction[0]['labels']).tolist()
            labels = [str(label) for label in labels]
            return jsonify({'boxes': boxes, 'lables': labels})


# Endpunkt zur Rückgabe des Bildes in dem die Weissen Fliegen markiert sind
@app.route('/predictimage', methods=['POST'])
def predictimage():
    if request.method == 'POST':
        file = request.files['file']
        if file is not None:
            input_tensor = image_preprocessing(file)
            image = get_prediction_on_image(input_tensor=input_tensor
                                            , prediction_tensor=get_prediction(input_tensor))
            # save and load image from/to buffer to have in in binary format
            buf = io.BytesIO()
            image.save(buf, format='JPEG')
            byte_img = buf.getvalue()
            return send_file(
                io.BytesIO(byte_img),
                mimetype='image/jpeg',
                attachment_filename="prediction.jpeg"
            )

if __name__ == '__main__':
    app.run()