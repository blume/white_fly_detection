import os
import xml.etree.ElementTree as ET

# Script zur Ausgabe von Informationen zum Datensatz
imgs = list(sorted(os.listdir("../sample_data/images")))
annotations =  list(sorted(os.listdir("../sample_data/annotations")))

wf_counter = 0
max_wf_on_img = 0
sub_counter = 0
nr_annos = 0
for annotation_file in annotations:
    tree = ET.parse("../sample_data/annotations/"+annotation_file)
    root = tree.getroot()
    for object in root.iter('object'):
            wf_counter += 1
            sub_counter += 1
    if sub_counter > max_wf_on_img:
        max_wf_on_img = sub_counter
    nr_annos += 1
    sub_counter = 0
print('Nr. Flys: ', wf_counter)
print('Max Flys on img: ', max_wf_on_img)
print('#anno files: ', nr_annos)


print(imgs)
print(annotations)

for img, annotation in zip(imgs, annotations):
    img = img.rstrip(".JPG")
    annotation = annotation.rstrip(".xml")
    if img != annotation:
        print("Not all images have an annotation or vice versa")
        print("error starts with image ", img, "and annoation", annotation)
        break

for img in imgs:
    annotation_found = False
    for annotation in annotations:
        img = img.rstrip(".JPG")
        annotation = annotation.rstrip(".xml")
        if img == annotation:
            annotation_found = True
            break
    if not annotation_found:
        print("image ", img, " has no annotation")

for annotation in annotations:
    img_found = False
    for img in imgs:
        img = img.rstrip(".JPG")
        annotation = annotation.rstrip(".xml")
        if img == annotation:
            img_found = True
            break
    if not img_found:
        print("annotation ", annotation, " has no image")