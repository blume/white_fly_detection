import torchvision
from model.BoxPredictor import BoxPredictor
from torchvision.models.detection.rpn import AnchorGenerator, RPNHead, RegionProposalNetwork
from training.HYPERPARAMS import *
from torchvision.ops import boxes as box_ops

# Laden und Anpassen des Deep Learning Modells
def get_pretrained_model(num_classes: int):
    # load a model pre-traind on coco
    model = torchvision.models.detection.faster_rcnn.fasterrcnn_resnet50_fpn(pretrained=True,
                                                                             trainable_backbone_layers=TRAINABLE_BACKBONE_LAYERS,
                                                                             min_size=MIN_IMAGE_SIZE,
                                                                             max_size=MAX_IMAGE_SIZE,
                                                                             box_score_thresh=BOX_SCORE_THRESH,
                                                                             box_nms_thresh=BOX_NMS_THRESH,
                                                                             )

    anchor_generator = AnchorGenerator(
        sizes=tuple([ANCHOR_SIZES for _ in range(5)]),
        aspect_ratios=tuple([ANCHOR_ASPECT_RATIOS for _ in range(5)]))
    model.rpn.anchor_generator = anchor_generator

    # 256 because that's the number of features that FPN returns
    model.rpn.head = RPNHead(256, anchor_generator.num_anchors_per_location()[0])

    # replace the classifier with a new one
    # get number of input features  for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = BoxPredictor(in_features, num_classes)
    return model



