from torch import nn
from training import HYPERPARAMS

# Pytorch Implementierung des BoxPredictors + Drop bei der Klassifizierung
class BoxPredictor(nn.Module):
    """
    Standard classification + bounding box regression layers
    for Fast R-CNN.

    Args:
        in_channels (int): number of input channels
        num_classes (int): number of output classes (including background)
    """

    def __init__(self, in_channels, num_classes):
        super(BoxPredictor, self).__init__()
        self.cls_score = nn.Linear(in_channels, num_classes)
        self.bbox_pred = nn.Linear(in_channels, num_classes * 4)
        self.dropout = nn.Dropout(HYPERPARAMS.DROPOUT)

    def forward(self, x):
        if x.dim() == 4:
            assert list(x.shape[2:]) == [1, 1]
        x = x.flatten(start_dim=1)
        scores = self.dropout(self.cls_score(x))
        bbox_deltas = self.bbox_pred(x)

        return scores, bbox_deltas

