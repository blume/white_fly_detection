import os
import sys
import time

import torch
import torch.utils.data
import numpy as np
import matplotlib.pyplot as plt
import torchvision.transforms.functional as F
from shutil import copyfile
from contextlib import redirect_stdout

from training.HYPERPARAMS import *
import training.utils
from training.WhiteFlyDataset import WhiteFlyDataset
from training.transformation import get_transform
from training.engine import train_one_epoch, evaluate
from training import utils

from model.pretrained_model import get_pretrained_model
#from model.pretrained_model import get_pretrained_model

# Skript in dem das Training statt findet

# funktion um ein Bild oder eine Liste von Bildern anzuzeigen
# welche bereits in einen Tensor umgewandelt wurden
def show(imgs):
    if not isinstance(imgs, list):
        imgs = [imgs]
    fix, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])


# Funktion um den Ordner anzulegen indem das trainierte modell
# und die dazugehörigen informationen gespeichert werden
def setup_output_dir() -> str:
    dir_name = os.path.join("../", "models", "model_" + time.strftime("%Y-%m-%d_%H-%M") + "/")
    os.mkdir(dir_name)
    copyfile("training/HYPERPARAMS.py", dir_name + "/HYPERPARAMS")
    return dir_name

# Das eigentliche Training
def main():
    # 1 class (whitefly) + background
    num_classes = 2
    # train on GPU or CPU if GPU isn't available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    if device.type == 'cpu':
        print('cuda needed for training')
        sys.exit()


    # Laden der datensätze
    dataset_train = WhiteFlyDataset('../sample_data', get_transform(train=True))
    dataset_test = WhiteFlyDataset('../sample_data', get_transform(train=False))

    # split dataset in test and train data
    indices = torch.randperm(len(dataset_train)).tolist()
    dataset_train = torch.utils.data.Subset(dataset_train, indices[:int(len(indices) * 0.8)])
    dataset_test = torch.utils.data.Subset(dataset_test, indices[int(len(indices) * 0.8):])

    data_loader_train = torch.utils.data.DataLoader(
        dataset_train, batch_size=BATCHSIZE, shuffle=True, num_workers=0,
        collate_fn=utils.collate_fn
    )

    data_loader_test = torch.utils.data.DataLoader(
        dataset_test, batch_size=1, shuffle=False, num_workers=0,
        collate_fn=utils.collate_fn
    )


    # Laden des Modells
    model = get_pretrained_model(num_classes)
    model.to(device)

    # construct optimizer
    # der optimizer passt die parameter an
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=0.005, momentum=0.9,
                                weight_decay=0.0005)

    # construct learning rate scheduler
    # Dadurch anpassung der Lernrate nach step_size an Trainingsdurchlaeufen
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                   step_size=3,
                                                   gamma=0.1)

    # Anzahl Trainingsdruchlaeufe
    num_epochs = 10
    train_history = []
    ap = []
    ar = []

    #Umleitung der Logausgabe in einen File
    model_dir = setup_output_dir()
    with open(model_dir + 'training_log', 'w') as f:
        with redirect_stdout(f):

            # Trainingsschleife
            for epoch in range(num_epochs):
                metric_logger = train_one_epoch(model, optimizer, data_loader_train, device, epoch,
                               print_freq=10)
                lr_scheduler.step()
                validation_metrics = evaluate(model, data_loader_test, device=device)
                torch.save(model.state_dict(), model_dir + "state-dict_epoch-" + str(epoch))
                train_history.append(metric_logger.meters)
                ap.append(validation_metrics.coco_eval['bbox'].stats[1])
                ar.append(validation_metrics.coco_eval['bbox'].stats[8])


            # Vorbereitung der Trainingsmetriken um einen Graph zu zeichen
            lr = []
            loss = []
            loss_classifier = []
            loss_box_reg = []
            loss_objectness = []
            loss_rpn_box_reg = []

            for epoch_metrics in train_history:
                lr.append(epoch_metrics['lr'].avg)
                loss.append(epoch_metrics['loss'].avg)
                loss_classifier.append(epoch_metrics['loss_classifier'].avg)
                loss_box_reg.append(epoch_metrics['loss_box_reg'].avg)
                loss_objectness.append(epoch_metrics['loss_objectness'].avg)
                loss_rpn_box_reg.append(epoch_metrics['loss_rpn_box_reg'].avg)

            # Figur in welche die Graphen geeichnet werden
            fig, (ax, validation_plot) = plt.subplots(2, 1)

            # Erstellen des Traingingsgraphen
            ax.plot(range(num_epochs), lr, color='tab:blue', label="lr")
            ax.plot(range(num_epochs), loss, color='tab:orange', label="loss")
            ax.plot(range(num_epochs), loss_classifier, color='tab:green', label="loss_classifier")
            ax.plot(range(num_epochs), loss_box_reg, color='tab:red', label="loss_box_reg")
            ax.plot(range(num_epochs), loss_objectness, color='tab:purple', label="loss_objectness")
            ax.plot(range(num_epochs), loss_rpn_box_reg, color='tab:cyan', label="loss_rpn_box_reg")

            # setzen der achesen
            ax.set_xlim([0, num_epochs - 1])
            ax.set_ylim([0, 1])
            ax.set_title('Training Losses')
            ax.legend(loc='lower right')

            # Erstellen des Validierungs Graph
            validation_plot.plot(range(num_epochs), ap, color='tab:red', label="AP, IoU 0.5")
            validation_plot.plot(range(num_epochs), ar, color='tab:blue', label="AR, IoU 0.5")
            validation_plot.set_xlim([0, num_epochs - 1])
            validation_plot.set_ylim([0,1])
            validation_plot.legend(loc='lower right')
            # Speichern der Graphen
            plt.savefig(model_dir + 'train_n_valid_plot.png')



main()
