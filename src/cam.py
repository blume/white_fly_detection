import warnings
warnings.filterwarnings('ignore')
warnings.simplefilter('ignore')
import requests
import resource
import cv2
import numpy as np
import torch
import torchvision
from torchvision.transforms import transforms
from pytorch_grad_cam import AblationCAM, EigenCAM
from pytorch_grad_cam.ablation_layer import  AblationLayerFasterRCNN
from pytorch_grad_cam.utils.model_targets import  FasterRCNNBoxScoreTarget
from pytorch_grad_cam.utils.reshape_transforms import fasterrcnn_reshape_transform
from pytorch_grad_cam.utils.image import show_cam_on_image, scale_accross_batch_and_channels, scale_cam_image
from PIL import Image, ImageOps
from model.pretrained_model import get_pretrained_model

def limit_memory(mem_percentage):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    print(soft, hard)
    resource.setrlimit(resource.RLIMIT_AS, ( mem_percentage, hard))
def image_preprocessing(img_file):
    input_transforms = [
        transforms.ToTensor()
    ]
    transformations = transforms.Compose(input_transforms)
    img = Image.open(img_file).convert('RGB')
    img = ImageOps.exif_transpose(img)
    transformed_img = transformations(img)
    return transformed_img


def predict(input_tensor, model, device, detection_threshold):
    outputs = model(input_tensor)
    pred_classes = [label_names[i] for i in outputs[0]['labels'].cpu().numpy()]
    pred_labels = outputs[0]['labels'].cpu().numpy()
    pred_scores = outputs[0]['scores'].detach().cpu().numpy()
    pred_bboxes = outputs[0]['boxes'].detach().cpu().numpy()

    boxes, classes, labels, indices = [], [], [], []
    for index in range(len(pred_scores)):
        if pred_scores[index] >= detection_threshold:
            boxes.append(pred_bboxes[index].astype(np.int32))
            classes.append(pred_classes[index])
            labels.append(pred_labels[index])
            indices.append(index)
    boxes = np.int32(boxes)
    return boxes, classes, labels, indices


def draw_boxes(boxes, labels, classes, image):
    for i, box in enumerate(boxes):
        color = COLORS[labels[i]]
        cv2.rectangle(image,
                      (int(box[0]), int(box[1])),
                      (int(box[2]), int(box[3])),
                      color, 2)
        cv2.putText(image, classes[i], (int(box[0]), int(box[1] - 5)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.8, color, 2,
                    lineType=cv2.LINE_AA)
    return image


label_names = ['__background__', 'white fly']

COLORS = np.random.uniform(0, 255, size=(len(label_names), 3))


def fasterrcnn_reshape_transform(x):
    target_size = x['pool'].size()[-2:]
    activations = []
    for key, value in x.items():
        activations.append(torch.nn.functional.interpolate(torch.abs(value),
                           target_size, mode='bilinear'))
    activations = torch.cat(activations, axis=1)
    return activations

#limit_memory(20510007705)
image_url = "../sample_data/images/_DSC5925.JPG"
#image = np.array(Image.open(requests.get(image_url, stream=True).raw))
image = np.array(Image.open(image_url))
####
#image = image_preprocessing("/home/fridolin/studium/thesis/whitefly_detection/dataset/images/_DSC5692.JPG")
####
image_float_np = np.float32(image) / 255
# define the torchvision image transforms
transform = torchvision.transforms.Compose([
    torchvision.transforms.ToTensor(),
])

input_tensor = transform(image)
#input_tensor = image
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
input_tensor = input_tensor.to(device)
# Add a batch dimension:
input_tensor = input_tensor.unsqueeze(0)

model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
model = get_pretrained_model(2)
model.load_state_dict(
    torch.load(
        "../models/model_2021-09-08_08-12/state-dict_epoch-5",
        map_location=device))
model.eval().to(device)
# Run the model and display the detections
boxes, classes, labels, indices = predict(input_tensor, model, device, 0.9)
image = draw_boxes(boxes, labels, classes, image)

#Image.fromarray(image).show()
Image.fromarray(image).save('detections.jpeg', format='jpeg')


target_layers = [model.backbone]
targets = [FasterRCNNBoxScoreTarget(labels=labels, bounding_boxes=boxes)]

cam = EigenCAM(model,
               target_layers,
               use_cuda=torch.cuda.is_available(),
               reshape_transform=fasterrcnn_reshape_transform)
greyscale_cam = cam(input_tensor, targets=targets)
greyscale_cam = greyscale_cam[0, :]
cam_image = show_cam_on_image(image_float_np, greyscale_cam, use_rgb=True)

image_with_bounding_boxes = draw_boxes(boxes, labels, classes,cam_image)
Image.fromarray(image_with_bounding_boxes).show()
Image.fromarray(image_with_bounding_boxes).save('eigen_cam.jpeg', format='jpeg')


#cam = AblationCAM(model,
#                  target_layers=target_layers,
#                  use_cuda=torch.cuda.is_available(),
#                  reshape_transform=fasterrcnn_reshape_transform,
#                  ablation_layer=AblationLayerFasterRCNN(),
#                  ratio_channels_to_ablate=0.01)
#greyscale_cam = cam(input_tensor, targets=targets)[0, :]

def renormalize_cam_in_bounding_boxes(boxes, image_float_np, grayscale_cam):
    """Normalize the CAM to be in the range [0, 1]
    inside every bounding boxes, and zero outside of the bounding boxes. """
    renormalized_cam = np.zeros(grayscale_cam.shape, dtype=np.float32)
    for x1, y1, x2, y2 in boxes:
        renormalized_cam[y1:y2, x1:x2] = scale_cam_image(grayscale_cam[y1:y2, x1:x2].copy())
    renormalized_cam = scale_cam_image(renormalized_cam)
    eigencam_image_renormalized = show_cam_on_image(image_float_np, renormalized_cam, use_rgb=True)
    image_with_bounding_boxes = draw_boxes(boxes, labels, classes, eigencam_image_renormalized)
    return image_with_bounding_boxes

#Image.fromarray(renormalize_cam_in_bounding_boxes(boxes, image_float_np, greyscale_cam)).show()


