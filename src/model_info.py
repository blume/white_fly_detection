from training import HYPERPARAMS
from model.pretrained_model import get_pretrained_model
from torchinfo import summary

# Script zur Ausgabe der Struktur des Deep Learning Modells
model = get_pretrained_model(2)
print(summary(model, input_size=(HYPERPARAMS.BATCHSIZE, 3, HYPERPARAMS.MIN_IMAGE_SIZE, HYPERPARAMS.MIN_IMAGE_SIZE)))
