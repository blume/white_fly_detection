import torch
from model.pretrained_model import get_pretrained_model
from torchvision.transforms import transforms
from PIL import Image, ImageOps
from torchvision.transforms.functional import to_pil_image


# Skript zur visualierung der Feautre Maps
def image_preprocessing(img_file):
    input_transforms = [
        transforms.ToTensor()
    ]
    transformations = transforms.Compose(input_transforms)
    img = Image.open(img_file).convert('RGB')
    img = ImageOps.exif_transpose(img)
    transformed_img = transformations(img)
    return transformed_img


img = image_preprocessing("../sample_data/images/_DSC5925.JPG")
print("img shape: ", img.shape)
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
model = get_pretrained_model(2)
model.load_state_dict(
    torch.load("../models/model_2021-09-08_08-12/state-dict_epoch-5", map_location=device))
model.eval()
with torch.no_grad():
    input_backbone, _ = model.transform([img])
    print(input_backbone.tensors.shape)
    #img = input_backbone.tensors.squeeze()
    #img = img.mul(255)
    #img = to_pil_image(img).convert('RGB')
    #img.show()
    #img.save("preprocessed_image.jpg")
    out_resnet = model.backbone.body(input_backbone.tensors)
    #img = out_resnet['0'].squeeze()[125]
    #img = to_pil_image(img)
    #img.convert('RGB').show()
    #img.save("backbone.png")
    k = 0
    for feature in out_resnet.values():
        feature = feature.squeeze()
        feature = torch.index_select(feature, 0, torch.tensor([0])).squeeze()
        print(feature.shape)
        feature = to_pil_image(feature)
        #feature.show()
        feature.save("feature_layer" + str(k) +".png")
        k += 1


    #quit()
    output_backbone = model.backbone.fpn(out_resnet)
    print("features", len(output_backbone.values()))
    k=0
    for key, feature in output_backbone.items():
            feature = feature.squeeze()
            feature = torch.index_select(feature, 0, torch.tensor([0])).squeeze()
            print(feature.shape)
            feature = to_pil_image(feature)
            #feature.show()
            feature.save("fpn_layer" + str(key) +".png")
            k += 1
    print("feature 0 ", output_backbone['1'].shape)
    quit()
    boxes_rpn, losses_rpn = model.rpn(images=input_backbone, features=output_backbone)
    print(boxes_rpn)
    print(boxes_rpn[0].shape)
    output_roiheads, _ = model.roi_heads(features=output_backbone,
                                      proposals=boxes_rpn,
                                      image_shapes=input_backbone.image_sizes, )

    detections = model.transform.postprocess(output_roiheads, input_backbone.image_sizes, [(4000, 6000)])
    print(detections)
    print(len(output_backbone))
    print(output_backbone['0'].shape)
    print(output_backbone['1'].shape)
    print(output_backbone['2'].shape)
    print(output_backbone['3'].squeeze().shape)
    print(output_backbone['pool'].shape)
    img = output_backbone['0'].squeeze()
    img = torch.index_select(img, 0, torch.tensor([0])).squeeze()
    img = to_pil_image(img)
    img.show()
    # print(output_backbone[0][255])
quit()
features = []


def save_features(mod, inp, outp):
    features.append(outp)


# you can also hook layers inside the roi_heads
layer_to_hook = 'backbone.fpn'
for name, layer in model.named_modules():
    print(name)
    if name == layer_to_hook:
        layer.register_forward_hook(save_features)

quit()
with torch.no_grad():
    prediction = model([img])

print(len(features))
print(type(features).__name__)
print(len(features[0]))
print(type(features[0]).__name__)
print(features[0])
# features[0].shape
for name in features[0]:
    print(name, features[0][name])

print(features[0]['0'].shape)
print(features[0]['0'])
featuemap0 = to_pil_image(features[0]['0'])
featuemap0 = featuemap0.convert('RGB')
featuemap0.show()
