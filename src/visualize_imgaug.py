from torchvision.transforms import ColorJitter
from PIL import Image, ImageOps

## TODO
## augment by HYPERPARAMS file not hardcoded values

# Skript zur Visualisierung der Image Augmentation

img = Image.open("../sample_data/images/_DSC5925.JPG").convert('RGB')
img = ImageOps.exif_transpose(img)

colorJitter = ColorJitter(brightness=0.5, contrast=0.25, saturation=0.5, hue=0.025)
imgj = colorJitter(img)
imgj = imgj.convert('RGB')
imgj = ImageOps.exif_transpose(imgj)
#img.show()
imgj.show()
#img.save('oriimg.png')
imgj.save('augimg.jpg')
