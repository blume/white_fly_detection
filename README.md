# white_fly_detection
## Why ##
In context of crop production, insect pest eat plant parts,
transmit diseases or suck plant juices. Currently, in
the three most important cereals insect pests causing losses of between 5% and 20%.
In addition, it is assumed that the insect pest burden will become more severe as a result of
the climate change.
By monitoring the insect pest infestation, problematic infestations
can be detected earlier and combated in a more targeted manner. In conventional agriculture, this means that fewer pesticides have to be used, which reduces the burden on the environment.

## What ##
There are lots of ways haw to implement pest monitoring solutions: Integrate it in a farm bot, build automated traps, use a smartphone app for scanning traps. For first and second, normal or hyperspectral cameras could be used. Then the data from the detection system should be fed into a mobile or desktop app, where farmers can see where, how many and what kind of insect pest were detected. If the amount of detections overcame a specific value, an alert with suggestions for fighting the insect pest should be triggered.

However, throw lack of time and money I decided only to build a prototype which could detect one kind of insect pest (white fly) on normal photos of yellow sticky traps and make it accessible on a web API for integration in a complete application.

## How ##
### The Deep Learning Network ###
For the detection of the white flies, I used a Faster-RCNN with FPN, a Deep Learning Network for real time object detection.
To train the network I wrote scripts with pytorch, therefore is training is independent of any could service provider and could run on easily on any GPU server with the right CUDA version installed.
### The Dataset ###
Since there aren't any datasets available, I created my own.
I distributed yellow sticky traps in greenhouses and made photos of them and labeled the white flies, with lableimg.
I made all photos in roughly the same distance with a resolution of 6000x4000pixels.
I do not expect that white flies could be detected on a different background in a different distance or with other resolution.
The Dataset is too big to upload it to git, so I uploaded only a few samples.
### The Webserver ###
A minimal python server, programmed with the flask framework.
One endpoint to mark the withe files on an image, another one to get a JSON file which describes where white flies are detected on the image.

## Run on your own ##
### Training ###
1. cd src
2. python3 -m venv -p /path/to/python3.8 venv_training/
3. source veenv_training/bin/activate
4. pip install -r requrements/training.txt
5. python train_model.py

The models are saved in the models folder together with the training log, plots of the training process and the hyperparameters used for training.
Since the models are binary data and need much space, I suggest keep them out of the git and use DVC to track changes. I uploaded my best model in the git trough lack of a cloud storage
#### Helpers ####
1. cd src
2. python3 -m venv -p /path/to/python3.8 venv_training/
3. source veenv_training/bin/utils
4. pip install -r requrements/utils.txt

* cam: creates a class activation map
* visalize_fp: creates output images from the cnn and the fpn
* visualize_imgaug: prints augmented images and is there to test image augmentation methods
* model_info: print structure of the model
* dataset_info: print information of the dataset, especially if every image has an annotation file. If not, the training script would assign wrong annotation files to images and then the training would be useless.

### Server ###
1. cd src
2. python3 -m venv -p /path/to/python3.8 venv_training/
3. source veenv_training/bin/server
4. pip install -r requrements/server.txt
5. python 3.8 server.py

* Example usage: Draw predictions on image:

	curl -X POST -H
”Content-Type: multipart/form-data”http://localhost:5000/predictimage -F
”file=@img.JPG”

* Example usage: Get predictions as JSON
	
	curl -X POST -H ”Content-Type:multipart/form-data” http://localhost:5000/predictjson -F ”file=@img.JPG”

## Continuation ##
I started this project as my bachelor thesis and continued the work during the KI-Garage Network Accelerator and now working on it in my spare time.
While there could be done much work improving the model and refactoring the training scripts or even switch to a higher level framework. There is more important work to do.
### End user Application ###
It very unlikely that farm will use curl to access web API to do some work on pest detection. Therefore, an end user application have to be developed.
I'm currently working on a small flutter app and searching a good (cheap or better, free) method to deploy the model/server.
This will be an early prototype to get in touch with farmers and show them what's possible.
Then in consultation with the farmers the app could be completed.
### Automated Scanning ###
It's also very unlikely the farmers or their workers will walk all over their greenhouses or farms to scan sticky traps. Therefore some solution for automated scanning have to be found.
I will propose automated (light) traps doing a count on trapped insects once a day or integration in a farm robot.
### Other Use-Cases, Ideas ... ###
This is currently all i got if you are intrested in this work have use-cases or just want to hack on a little bit feel free to contact me.
